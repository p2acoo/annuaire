using annuaire.Services.Hash;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace annuaire.Services.Employee;

public class EmployeeService : IEmployeeService
{
    private readonly AnnuaireContext _context;
    private readonly IHashService _hashService;

    public EmployeeService(AnnuaireContext context, IHashService hashService)
    {
        _context = context;
        _hashService = hashService;
    }

    public async Task<ActionResult<List<Models.Employee>>> ListAllEmployees(int page)
    {
        return await _context.Employee.Skip((page - 1) * 10).Take(10).ToListAsync();
    }

    public async Task<ActionResult<int>> GetNumberPage()
    {
        ActionResult<int> pages = await _context.Employee.CountAsync();
        return (int)Math.Ceiling((double)pages.Value / 10);
    }

    public async Task<ActionResult<Models.Employee>> GetEmployeeById(int id)
    {
        ActionResult<Models.Employee?> employee =
            await _context.Employee.Include(e => e.Service)
                .Include(e => e.Site)
                .FirstAsync(e => e.Id == id);
        if (employee.Value == null) return new NotFoundResult();
        return employee.Value;
    }

    public async Task<ActionResult<Models.Employee>> CreateEmployee(Models.Employee employee)
    {
        _context.Employee.Add(employee);
        await _context.SaveChangesAsync();
        return employee;
    }

    public async Task<ActionResult<Models.Employee>> UpdateEmployee(Models.Employee employee)
    {
        ActionResult<Models.Employee?> employeeToUpdate =
            await _context.Employee.Include(e => e.Service)
                .Include(e => e.Site)
                .FirstAsync(e => e.Id == employee.Id);
        if (employeeToUpdate.Value == null)
        {
            return new NotFoundResult();
        }

        employeeToUpdate.Value.LastName = employee.LastName;
        employeeToUpdate.Value.FirstName = employee.FirstName;
        employeeToUpdate.Value.Email = employee.Email;
        employeeToUpdate.Value.FixedPhone = employee.FixedPhone;
        employeeToUpdate.Value.MobilePhone = employee.MobilePhone;
        employeeToUpdate.Value.Service = employee.Service;
        employeeToUpdate.Value.Site = employee.Site;
        if (employee.Password != null)
        {
            //password is a SHA512 hash
            var password = _hashService.GetHash(employee.Password);
            employeeToUpdate.Value.Password = password;
        }

        await _context.SaveChangesAsync();
        return employeeToUpdate.Value;
    }

    public async Task DeleteEmployee(Models.Employee employee)
    {
        _context.Employee.Remove(employee);
        await _context.SaveChangesAsync();
    }

    public async Task<(ActionResult<List<Models.Employee>>, int)> ListWithFilters(Dictionary<string, string> filters,
        int page)
    {
        IQueryable<Models.Employee> employees = _context.Employee.Include(
            e => e.Service).Include(e => e.Site);
        //list of filters : service, site, LastName, FirstName, email, FixedPhone, MobilePhone
        if (filters.ContainsKey("Service") && filters["Service"] != "")
        {
            employees = employees.Where(e => e.Service.Id == int.Parse(filters["Service"].Trim()));
        }

        if (filters.ContainsKey("Site") && filters["Site"] != "")
        {
            employees = employees.Where(e => e.Site.Id == int.Parse(filters["Site"].Trim()));
        }

        if (filters.ContainsKey("LastName"))
        {
            employees = employees.Where(e => e.LastName.ToLower().Contains(filters["LastName"].ToLower().Trim()));
        }

        if (filters.ContainsKey("FirstName"))
        {
            employees = employees.Where(e => e.FirstName.ToLower().Contains(filters["FirstName"].ToLower().Trim()));
        }

        if (filters.ContainsKey("Email"))
        {
            employees = employees.Where(e => e.Email.ToLower().Contains(filters["Email"].ToLower().Trim()));
        }

        if (filters.ContainsKey("FixedPhone"))
        {
            employees = employees.Where(e => e.FixedPhone.ToLower().Contains(filters["FixedPhone"].ToLower().Trim()));
        }

        if (filters.ContainsKey("MobilePhone"))
        {
            employees = employees.Where(e => e.MobilePhone.ToLower().Contains(filters["MobilePhone"].ToLower().Trim()));
        }

        int NumberOfPages = (int)Math.Ceiling((double)employees.Count() / 10);
        employees = employees.Skip((page - 1) * 10).Take(10);
        return (await employees.ToListAsync(), NumberOfPages);
    }

    public async Task<bool> GetEmployeeByEmail(string email)
    {
        return await _context.Employee.AnyAsync(e => e.Email.ToLower().Trim() == email.ToLower().Trim());
    }

    public async Task<bool> GetEmployeeByEmailAndId(string email, int id)
    {
        return await _context.Employee.AnyAsync(e =>
            e.Email.ToLower().Trim() == email.ToLower().Trim() && e.Id.ToString().ToLower() != id.ToString().ToLower());
    }
}