using Microsoft.AspNetCore.Mvc;

namespace annuaire.Services.Employee;

public interface IEmployeeService
{
    public Task<ActionResult<Models.Employee>> GetEmployeeById(int id);
    public Task<ActionResult<Models.Employee>> CreateEmployee(Models.Employee employee);
    public Task<ActionResult<Models.Employee>> UpdateEmployee(Models.Employee employee);
    public Task DeleteEmployee(Models.Employee employee);

    public Task<(ActionResult<List<Models.Employee>>, int)> ListWithFilters(Dictionary<string, string> filters,
        int page);

    public Task<bool> GetEmployeeByEmail(string email);
    public Task<bool> GetEmployeeByEmailAndId(string email, int id);
}