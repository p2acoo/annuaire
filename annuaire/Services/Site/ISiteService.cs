using Microsoft.AspNetCore.Mvc;

namespace annuaire.Services.Site;

public interface ISiteService
{
    public Task<ActionResult<List<Models.Site>>> ListAllSite(int page);
    public Task<ActionResult<int>> GetNumberPage();
    public Task<ActionResult<Models.Site>> GetSiteById(int id);
    public Task<IActionResult> CreateSite(Models.Site site);
    public Task<IActionResult> UpdateSite(Models.Site site);
    public Task DeleteSite(Models.Site site);
    public Task<bool> IsSiteAlreadyExist(Models.Site site);
    public Task<ActionResult<List<Models.Site>>> ListAllSiteWithoutPagination();
}