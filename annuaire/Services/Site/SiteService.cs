using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace annuaire.Services.Site;

public class SiteService : ISiteService
{
    private readonly AnnuaireContext _context;
    
    public SiteService(AnnuaireContext context)
    {
        _context = context;
    }
    
    public async Task<ActionResult<List<Models.Site>>> ListAllSite(int page)
    {
        return await _context.Site.Skip((page - 1) * 10).Take(10).ToListAsync();
    }

    public async Task<ActionResult<int>> GetNumberPage()
    {
        ActionResult<int> pages = await _context.Site.CountAsync();
        return (int)Math.Ceiling((double)pages.Value / 10);
    }

    public async Task<ActionResult<Models.Site>> GetSiteById(int id)
    {
        ActionResult<Models.Site?> site =
            await _context.Site.Include(e => e.Employee)
                .FirstAsync(e => e.Id == id);
        if (site.Value == null) return new NotFoundResult();
        return site.Value;
    }

    public async Task<IActionResult> CreateSite(Models.Site site)
    {
        _context.Site.Add(site);
        await _context.SaveChangesAsync();
        return new CreatedAtActionResult("GetSiteById", "Site", new {id = site.Id}, site);
    }

    
    public async Task<IActionResult> UpdateSite([FromForm]Models.Site site)
    {
        var siteToUpdate = await _context.Site.FindAsync(site.Id);
        if (siteToUpdate == null)
        {
            return new NotFoundResult();
        }
        siteToUpdate.City = site.City;
        siteToUpdate.Description = site.Description;
        await _context.SaveChangesAsync();
        return new NoContentResult();
    }

    public async Task DeleteSite(Models.Site site)
    {
        _context.Site.Remove(site);
        await _context.SaveChangesAsync();
    }
    
    public async Task<bool> IsSiteAlreadyExist(Models.Site site)
    {
        return await _context.Site.AnyAsync(s => s.City.Trim().ToLower() == site.City.Trim().ToLower() && s.Description.Trim().ToLower() == site.Description.Trim().ToLower());
    }

    public async Task<ActionResult<List<Models.Site>>> ListAllSiteWithoutPagination()
    {
        return await _context.Site.ToListAsync();
    }
}
