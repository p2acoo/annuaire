using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace annuaire.Services.Service;

public class ServiceService : IServiceService
{
    private readonly AnnuaireContext _context;
    
    public ServiceService(AnnuaireContext context)
    {
        _context = context;
    }
    
    public async Task<ActionResult<List<Models.Service>>> ListAllServices(int page)
    {
        return await _context.Service.Skip((page - 1) * 10).Take(10).ToListAsync();
    }
    public async Task<ActionResult<List<Models.Service>>> ListAllServicesWithName(string name)
    {
        return await _context.Service.Where(e => e.Name.ToLower().Trim().Contains(name)).ToListAsync();
    }

    public async Task<ActionResult<int>> GetNumberPage()
    {
        ActionResult<int> pages = await _context.Service.CountAsync();
        return (int)Math.Ceiling((double)pages.Value / 10);
    }

    public async Task<ActionResult<Models.Service>> GetServiceById(int id)
    {
        ActionResult<Models.Service?> service =
            await _context.Service.Include(e => e.Employees)
                .FirstAsync(e => e.Id == id);
        if (service.Value == null) return new NotFoundResult();
        return service.Value;
    }

    public async Task<IActionResult> CreateService(Models.Service service)
    {
         _context.Service.Add(service);
        await _context.SaveChangesAsync();
        return new CreatedAtActionResult("GetServiceById", "Service", new {id = service.Id}, service);
    }

    public async Task<IActionResult> UpdateService(Models.Service service)
    {
        var serviceToUpdate = await _context.Service.FindAsync(service.Id);
        if (serviceToUpdate == null)
        {
            return new NotFoundResult();
        }
        serviceToUpdate.Name = service.Name;
        await _context.SaveChangesAsync();
        return new NoContentResult();
    }

    public async Task DeleteService(Models.Service service)
    {
        _context.Service.Remove(service);
        await _context.SaveChangesAsync();
    }
    
    public async Task<bool> IsServiceAlreadyExiste(Models.Service service)
    {
        return await _context.Service.AnyAsync(e => e.Name.Trim().ToLower() == service.Name.Trim().ToLower());
    }
    
    public async Task<ActionResult<List<Models.Service>>> ListAllServicesWithoutPagination()
    {
        return await _context.Service.ToListAsync();
    }
}