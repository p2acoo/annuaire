using Microsoft.AspNetCore.Mvc;

namespace annuaire.Services.Service;

public interface IServiceService
{
    public Task<ActionResult<List<Models.Service>>> ListAllServices(int page);
    public Task<ActionResult<int>> GetNumberPage();
    public Task<ActionResult<Models.Service>> GetServiceById(int id);
    public Task<IActionResult> CreateService(Models.Service service);
    public Task<IActionResult> UpdateService(Models.Service service);
    public Task DeleteService(Models.Service service);
    public Task<bool> IsServiceAlreadyExiste(Models.Service service);
    public Task<ActionResult<List<Models.Service>>> ListAllServicesWithName(string name);
    public Task<ActionResult<List<Models.Service>>> ListAllServicesWithoutPagination();
}