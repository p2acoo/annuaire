using Microsoft.AspNetCore.Mvc;

public class ErrorController : Controller
{
    //error handler
    public IActionResult Index(string? statusCode)
    {
        if (statusCode == null)
        {
            return View();
        }

        return statusCode switch
        {
            "404" => View("NotFound"),
            "403" => View("Forbidden"),
            "500" => View("InternalServerError"),
            "400" => View("BadRequest"),
            "401" => View("Unauthorized"),
            _ => View()
        };
    }
}