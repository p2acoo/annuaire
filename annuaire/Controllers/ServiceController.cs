using annuaire.Models;
using annuaire.Services.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace annuaire.Controllers;

public class ServiceController : Controller
{
    private readonly IServiceService _serviceService;

    public ServiceController(IServiceService serviceService)
    {
        _serviceService = serviceService;
    }

    //list all services
    public async Task<IActionResult> Index(int page = 1)
    {
        ActionResult<List<Service>> services = await _serviceService.ListAllServices(page);
        ActionResult<int> pages = await _serviceService.GetNumberPage();
        ViewBag.NumberPage = pages.Value;
        return View(services.Value);
    }

    [HttpGet]
    public async Task<IActionResult> Update(int id)
    {
        ActionResult<Service> site = await _serviceService.GetServiceById(id);
        return View(site.Value);
    }

    [HttpPost]
    public async Task<IActionResult> Update(int id, Service service)
    {
        if (id != service.Id)
        {
            return new BadRequestResult();
        }

        ActionResult<Service> serviceToUpdate = await _serviceService.GetServiceById(id);
        if (serviceToUpdate.Value == null)
        {
            return new NotFoundResult();
        }

        if (!ModelState.IsValid)
        {
            return Redirect($"/Service/Get/{service.Id}");
        }

        try
        {
            bool exist = await _serviceService.IsServiceAlreadyExiste(service);
            if (exist)
            {
                ModelState.AddModelError("Name", "Ce nom est déjà utilisé.");
                return View(service);
            }

            await _serviceService.UpdateService(service);
        }
        catch (DbUpdateConcurrencyException)
        {
            return new NotFoundResult();
        }

        return Redirect($"/Service/Get/{service.Id}");
    }

    [HttpGet]
    public IActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Create(Service service)
    {
        if (!ModelState.IsValid)
        {
            return View(service);
        }

        try
        {
            bool exist = await _serviceService.IsServiceAlreadyExiste(service);
            if (exist)
            {
                ModelState.AddModelError("Name", "Ce nom est déjà utilisé.");
                return View(service);
            }

            await _serviceService.CreateService(service);
        }
        catch (DbUpdateConcurrencyException)
        {
            return new NotFoundResult();
        }

        return RedirectToAction("Index");
    }

    [HttpPost]
    [Route("Service/Delete/{id:int}")]
    [Authorize]
    public async Task<IActionResult> Delete(int id)
    {
        ActionResult<Service> service = await _serviceService.GetServiceById(id);

        if (service.Value == null)
        {
            return new NotFoundResult();
        }

        if (service.Value.Employees!.Count == 0)
        {
            await _serviceService.DeleteService(service.Value);
        }
        else
        {
            return Redirect("/Service/Index");
        }

        return Redirect("/Service/Index");
    }
}