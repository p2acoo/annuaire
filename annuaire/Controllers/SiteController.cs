using annuaire.Models;
using annuaire.Services.Site;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace annuaire.Controllers;

public class SiteController : Controller
{
    private readonly ISiteService _siteService;

    public SiteController(ISiteService siteService)
    {
        _siteService = siteService;
    }

    public async Task<IActionResult> Index(int page = 1)
    {
        ActionResult<List<Site>> site = await _siteService.ListAllSite(page);
        ActionResult<int> pages = await _siteService.GetNumberPage();
        ViewBag.NumberPage = pages.Value;
        return View(site.Value);
    }

    [HttpGet]
    public async Task<IActionResult> Update(int id)
    {
        ActionResult<Site> site = await _siteService.GetSiteById(id);
        return View(site.Value);
    }

    [HttpPost]
    public async Task<IActionResult> Update(int id, Site site)
    {
        if (id != site.Id)
        {
            return new BadRequestResult();
        }

        ActionResult<Site> siteToUpdate = await _siteService.GetSiteById(id);
        if (siteToUpdate.Value == null)
        {
            return new NotFoundResult();
        }

        if (!ModelState.IsValid)
        {
            return View(site);
        }

        try
        {
            bool exist = await _siteService.IsSiteAlreadyExist(site);
            if (exist)
            {
                ModelState.AddModelError("City", "Ce site existe déjà");
                    return View(site);
            }

            await _siteService.UpdateSite(site);
        }
        catch (DbUpdateConcurrencyException)
        {
            return new NotFoundResult();
        }

        return View(site);
    }

    [HttpGet]
    public IActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Create(Site site)
    {
        if (!ModelState.IsValid)
        {
            return View(site);
        }

        try
        {
            bool exist = await _siteService.IsSiteAlreadyExist(site);
            if (exist)
            {
                ModelState.AddModelError("City", "Ce site existe déjà");
                return View(site);
            }

            await _siteService.CreateSite(site);
        }
        catch (DbUpdateConcurrencyException)
        {
            return new NotFoundResult();
        }

        return Redirect($"/Site/Get/{site.Id}");
    }

    [HttpPost]
    [Route("Site/Delete/{id:int}")]
    [Authorize]
    public async Task<IActionResult> Delete(int id)
    {
        ActionResult<Site> site = await _siteService.GetSiteById(id);
        if (site.Value == null)
        {
            return new NotFoundResult();
        }

        //to not delete if there remaning user
        if (site.Value.Employee!.Count == 0)
        {
            await _siteService.DeleteSite(site.Value);
        }
        else
        {
            return Redirect("/Site/Index");
        }

        return Redirect("/Site/Index");
    }
}