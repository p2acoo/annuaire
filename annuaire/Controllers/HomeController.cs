﻿using System.Security.Claims;
using annuaire.Models;
using annuaire.Models.Auth;
using annuaire.Services.Hash;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace annuaire.Controllers;

public class HomeController : Controller
{
    private readonly AnnuaireContext _context;
    private readonly IHashService _hashService;

    public HomeController(AnnuaireContext context, IHashService hashService)
    {
        _context = context;
        _hashService = hashService;
    }

    public IActionResult Index()
    {
        return View();
    }

    [Authorize]
    public IActionResult Privacy()
    {
        return View();
    }

    public IActionResult Login()
    {
        return View();
    }

    [HttpPost]
    public async Task<ActionResult> Login(Login login)
    {
        if (!ModelState.IsValid) return View(login);
        Employee? user = await _context.Employee.FirstOrDefaultAsync(u =>
            u.Email == login.Email && u.Password == _hashService.GetHash(login.Password) && u.IsAdmin);
        ModelState.AddModelError("Email", "Email ou mot de passe incorrect");
        if (user == null) return View(login);

        var claims = new List<Claim>
        {
            new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            new Claim(ClaimTypes.Name, user.FirstName + " " + user.LastName),
            new Claim(ClaimTypes.Email, user.Email),
            new Claim(ClaimTypes.Role, user.IsAdmin ? "Admin" : "User")
        };

        var claimsIdentity = new ClaimsIdentity(
            claims, CookieAuthenticationDefaults.AuthenticationScheme);

        var principal = new ClaimsPrincipal(claimsIdentity);
        var props = new AuthenticationProperties
        {
            IsPersistent = true,
            ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10)
        };

        HttpContext.SignInAsync(
            CookieAuthenticationDefaults.AuthenticationScheme,
            principal,
            props).Wait();

        return RedirectToAction("Index", "Home");
    }

    public IActionResult Logout()
    {
        HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        return RedirectToAction("Login", "Home");
    }
}