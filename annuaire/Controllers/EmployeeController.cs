using annuaire.Models;
using annuaire.Services.Employee;
using annuaire.Services.Service;
using annuaire.Services.Site;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace annuaire.Controllers;

public class EmployeeController : Controller
{
    //import services
    private readonly IEmployeeService _employeeService;
    private readonly IServiceService _serviceService;
    private readonly ISiteService _siteService;

    public EmployeeController(IEmployeeService employeeService, IServiceService serviceService,
        ISiteService siteService)
    {
        _employeeService = employeeService;
        _siteService = siteService;
        _serviceService = serviceService;
    }

    [HttpGet]
    public async Task<IActionResult> Update(int id)
    {
        ActionResult<Employee> employee = await _employeeService.GetEmployeeById(id);
        var services = await _serviceService.ListAllServicesWithoutPagination();
        if (services.Value != null)
        {
            ViewBag.Services = services.Value;
        }

        var sites = await _siteService.ListAllSiteWithoutPagination();
        if (sites.Value != null)
        {
            ViewBag.Sites = sites.Value;
        }

        return View(employee.Value);
    }

    [HttpPost]
    public async Task<IActionResult> Update([FromForm] int id, Employee employee)
    {
        if (id != employee.Id)
        {
            return View(employee);
        }

        var services = await _serviceService.ListAllServicesWithoutPagination();
        //to send the list of services to the view
        if (services.Value != null)
        {
            ViewBag.Services = services.Value;
        }

        var sites = await _siteService.ListAllSiteWithoutPagination();
        if (sites.Value != null)
        {
            ViewBag.Sites = sites.Value;
        }

        ActionResult<Employee> employeeToUpdate = await _employeeService.GetEmployeeById(id);
        if (employeeToUpdate.Value == null)
        {
            return new NotFoundResult();
        }

        var serviceId = Request.Form["Service"];
        var siteId = Request.Form["Site"];
        if (serviceId != "" && siteId != "")
        {
            var service = await _serviceService.GetServiceById(int.Parse(serviceId));
            if (service.Value == null)
            {
                ModelState.AddModelError("Service", "Le service est nécessaire");
                return View(employee);
            }

            var site = await _siteService.GetSiteById(int.Parse(siteId));
            if (site.Value == null)
            {
                ModelState.AddModelError("Site", "Le site est nécessaire");
                return View(employee);
            }

            employee.Service = service.Value;
            employee.Site = site.Value;
        }
        else
        {
            ModelState.AddModelError("Service", "Le service est nécessaire");
            ModelState.AddModelError("Site", "Le site est nécessaire");
            return View(employee);
        }

        if (!ModelState.IsValid)
        {
            return View(employee);
        }

        try
        {
            bool exist = await _employeeService.GetEmployeeByEmailAndId(employee.Email, employee.Id);
            if (exist)
            {
                ModelState.AddModelError("Email", "Cette email existe déjà");
                return View(employee);
            }

            await _employeeService.UpdateEmployee(employee);
        }
        catch (DbUpdateConcurrencyException)
        {
            return new NotFoundResult();
        }


        return View(employee);
    }

    [HttpGet]
    public async Task<IActionResult> Search()
    {
        var queryDict = new Dictionary<string, string>();
        foreach (var key in Request.Query)
        {
            queryDict.Add(key.Key, key.Value!);
        }

        int page = queryDict.ContainsKey("page") ? int.Parse(queryDict["page"]) : 1;
        var result = await _employeeService.ListWithFilters(queryDict, page);

        var services = await _serviceService.ListAllServicesWithoutPagination();
        if (services.Value != null)
        {
            ViewBag.Services = services.Value;
        }

        var sites = await _siteService.ListAllSiteWithoutPagination();
        if (sites.Value != null)
        {
            ViewBag.Sites = sites.Value;
        }

        ViewBag.NumberPage = result.Item2;
        ViewBag.Page = page;
        return View(result.Item1.Value);
    }

    [HttpPost]
    [Route("Employee/Delete/{id:int}")]
    [Authorize]
    public IActionResult Delete(int id)
    {
        var token = HttpContext.User.Identities.First().Claims.First().Value;
        ActionResult<Employee> currentEmployee = _employeeService.GetEmployeeById(int.Parse(token)).Result;
        if (currentEmployee.Value == null)
        {
            return new NotFoundResult();
        }

        if (currentEmployee.Value.Id == id)
        {
            return new BadRequestResult();
        }

        ActionResult<Employee> employee = _employeeService.GetEmployeeById(id).Result;

        if (employee.Value == null)
        {
            return new NotFoundResult();
        }

        _employeeService.DeleteEmployee(employee.Value);
        return Redirect("/Employee/Search");
    }

    [HttpGet]
    public async Task<IActionResult> Create()
    {
        var services = await _serviceService.ListAllServicesWithoutPagination();
        if (services.Value != null)
        {
            ViewBag.Services = services.Value;
        }

        var sites = await _siteService.ListAllSiteWithoutPagination();
        if (sites.Value != null)
        {
            ViewBag.Sites = sites.Value;
        }

        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Create([FromForm] Employee employee)
    {
        var serviceId = Request.Form["Service"];
        var siteId = Request.Form["Site"];
        //to send the list of services to the view
        if (serviceId != "" && siteId != "")
        {
            var service = await _serviceService.GetServiceById(int.Parse(serviceId));
            if (service.Value == null)
            {
                ModelState.AddModelError("Service", "Ce service n'existe pas");
                return View(employee);
            }

            var site = await _siteService.GetSiteById(int.Parse(siteId));
            if (site.Value == null)
            {
                ModelState.AddModelError("Site", "Ce site n'existe pas");
                return View(employee);
            }

            employee.Service = service.Value;
            employee.Site = site.Value;
        }
        else
        {
            var services = await _serviceService.ListAllServicesWithoutPagination();
            if (services.Value != null)
            {
                ViewBag.Services = services.Value;
            }

            var sites = await _siteService.ListAllSiteWithoutPagination();
            if (sites.Value != null)
            {
                ViewBag.Sites = sites.Value;
            }

            ModelState.AddModelError("Service", "Ce service n'existe pas");
            ModelState.AddModelError("Site", "Ce site n'existe pas");
            return View(employee);
        }

        if (!ModelState.IsValid)
        {
            var services = await _serviceService.ListAllServicesWithoutPagination();
            if (services.Value != null)
            {
                ViewBag.Services = services.Value;
            }

            var sites = await _siteService.ListAllSiteWithoutPagination();
            if (sites.Value != null)
            {
                ViewBag.Sites = sites.Value;
            }

            return View(employee);
        }

        try
        {
            bool exist = await _employeeService.GetEmployeeByEmail(employee.Email);
            if (exist)
            {
                var services = await _serviceService.ListAllServicesWithoutPagination();
                if (services.Value != null)
                {
                    ViewBag.Services = services.Value;
                }

                var sites = await _siteService.ListAllSiteWithoutPagination();
                if (sites.Value != null)
                {
                    ViewBag.Sites = sites.Value;
                }

                ModelState.AddModelError("Email", "Cette email existe déjà");
                return View(employee);
            }

            await _employeeService.CreateEmployee(employee);
        }
        catch (DbUpdateConcurrencyException)
        {
            return new NotFoundResult();
        }

        return Redirect("/Employee/Search");
    }
}