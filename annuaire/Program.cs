using annuaire;
using annuaire.Models;
using annuaire.Services.Hash;
using annuaire.Services.Employee;
using annuaire.Services.Service;
using annuaire.Services.Site;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(
    options =>
    {
        options.LoginPath = "/Home/login";
        options.Cookie.Name = "AnnuaireCookie";
        options.AccessDeniedPath = "/Home/Index";
    });
builder.Services.AddMvc();
builder.Services.AddScoped<IHashService, HashService>();
builder.Services.AddScoped<IEmployeeService, EmployeeService>();
builder.Services.AddScoped<IServiceService, ServiceService>();
builder.Services.AddScoped<ISiteService, SiteService>();


builder.Services.AddDbContext<AnnuaireContext>(
    options => options.UseNpgsql(builder.Configuration.GetConnectionString("AnnuaireContext")));

var app = builder.Build();
app.UseStatusCodePagesWithReExecute("/error/{0}");

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");

    app.UseStatusCodePagesWithReExecute("/404");

    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();
app.UseCookiePolicy();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
//for error
app.MapControllerRoute(
    name: "error",
    pattern: "/Error/{statusCode}",
    defaults: new { controller = "Error", action = "Index" });
app.Run();