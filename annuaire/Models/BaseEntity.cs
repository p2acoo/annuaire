namespace annuaire.Models;

public class BaseEntity
{
    //to have created at and updated at on entities
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
}