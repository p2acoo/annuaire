using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;

namespace annuaire.Models;

[PrimaryKey(nameof(Id))]
public class Employee : BaseEntity
{
    public int Id { get; set; }

    [Required(ErrorMessage = "Le prénom est obligatoire")]
    [StringLength(50, ErrorMessage = "Le prénom ne doit pas dépasser 50 caractères et minimum 2 caractères",
        MinimumLength = 2)]
    public string FirstName { get; set; }

    [Required]
    [StringLength(50, ErrorMessage = "Le nom ne doit pas dépasser 50 caractères et minimum 2 caractères",
        MinimumLength = 2)]
    public string LastName { get; set; }

    [Required]
    [EmailAddress(ErrorMessage = "L'adresse email n'est pas valide")]
    [StringLength(60, ErrorMessage = "L'email ne doit pas dépasser 60 caractères")]
    public string Email { get; set; }

    [Required(ErrorMessage = "Le téléphone portable est obligatoire")]
    [Phone(ErrorMessage = "Le numéro de téléphone portable n'est pas valide")]
    public string MobilePhone { get; set; }

    [Required(ErrorMessage = "Le téléphone fixe est obligatoire")]
    [Phone(ErrorMessage = "Le numéro de téléphone fixe n'est pas valide")]
    public string FixedPhone { get; set; }

    [NotMapped] public string? Password { get; set; }

    [Column(TypeName = "boolean")] public bool IsAdmin { get; set; }

    public Service? Service { get; set; }

    public Site? Site { get; set; }
}