using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace annuaire.Models;

[PrimaryKey(nameof(Id))]
public class Site : BaseEntity
{
    public int Id { get; set; }

    [Column(TypeName = "varchar(50)")]
    [StringLength(50, ErrorMessage = "La description du site ne soit pas dépasser 50 caractère et minimum 2 caractères",
        MinimumLength = 2)]
    public string Description { get; set; }

    [Column(TypeName = "varchar(50)")]
    [StringLength(50, ErrorMessage = "La ville du site ne soit pas dépasser 50 caractère et minimum 2 caractères",
        MinimumLength = 2)]
    public string City { get; set; }

    public ICollection<Employee?>? Employee { get; set; }
}