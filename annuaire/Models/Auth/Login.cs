using System.ComponentModel.DataAnnotations;

namespace annuaire.Models.Auth;

public class Login
{
    [Required]
    [EmailAddress(ErrorMessage = "L'adresse email n'est pas valide")]
    public string Email { get; set; }
    
    [Required]
    [DataType(DataType.Password)]
    public string Password { get; set; }
}
