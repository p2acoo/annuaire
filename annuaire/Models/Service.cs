using Microsoft.EntityFrameworkCore;

namespace annuaire.Models;

using System.ComponentModel.DataAnnotations;

[PrimaryKey(nameof(Id))]
public class Service : BaseEntity
{
    public int Id { get; set; }

    [Key]
    [StringLength(50, ErrorMessage = "Le nom du service ne soit pas dépasser 50 caractère et minimum 2 caractères",
        MinimumLength = 2)]
    public string Name { get; set; }

    public ICollection<Employee?>? Employees { get; set; }
}